# Say Something

# Say Something

An AI algorithm for generating speech audio

- **Property Title 1**: Property Description 1
- **Property Title 2**: Property Description 2
- **Property Title 3**: Property Description 3

🖼️ Watch a video introduction [here](https://youtube.com).

### Introduction
A long introduction to the project. A description of the problem. A possible solution. Another possible solution. A link to a more detailed exploration of the problem.

This project uses a combination of this and that solution to solve this problem.

### Installation
Installing the project from a command line interface (CLI)

```
npm install say-something
```

### Examples
Examples of how to use this project in your existing codebase.

#### How to do thing 1
```js
// Example use case 1
```

#### How to do thing 2
```js
// Example use case 2
```

### Example Use Cases
Examples of when to use this project and when not to use this project.

- Use when you want to do this.
- Use when you want to do that.

- Don't use if you want to do this other thing.
- Don't use if you want to do this other other thing.

### License
MIT
