# Problem Statement

Speech audio generation is easy, simply return an audio file of a random waveform.

But that's the problem, random waveforms don't usually sound like speech. But even with languages we're not familiar with, for example if you're a non-italian speaker, the italian speech patterns don't necessarily mean anything to you.

# Solution Statement

Say Something is an algorithm that uses speech audio and text to form a game world for itself.

The algorithm plays games within the game world to see if it can mimic certain sounds and predict what sound comes after another sound. Different games have different goals. Different games teach different things. Not doing well on a game means trying again later.
